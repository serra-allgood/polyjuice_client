0.4.1
=====

Features:

- Support the `whoami` endpoint. (Thanks to multi prise.)
- Add new `mix` tasks for joining and leaving rooms.

0.4.0
=====

Breaking changes:

- The `Polyjuice.Client.API` interface has a new `get_user_and_device`
  function; modules that implement the interface must implement this.
- A new field has been added to the `:limited` message: the message will
  contain the state current events from the sync.

Features:

- Add functions for getting/setting user profile and account data.  (Thanks to
  multi prise.)
- Add functions for creating rooms.  (Thanks to multi prise.)
- Add functions for setting read receipts/markers.  (Thanks to multi prise.)
- Allow low-level client to send the user ID as an Application Service.
  (Thanks to Pierre de Lacroix and multi prise.)
- Send ephemeral events to the handler.  (Thanks to Arjan Scherpenisse.)
- Send typing indicator.  (Thanks to Arjan Sherpenisse.)
- Resolve room aliases.  (Thanks to Arjan Sherpenisse.)
- Add function for adding mathematical notation to messages in `MsgBuilder`.

0.3.1
=====

Features:

- allow compressed server responses
- add support for specifying a proxy, SSL options, following redirects

0.3.0
=====

Breaking changes:

- The `Polyjuice.Client` module has been reworked.
  - Rather than creating a `Polyjuice.Client` struct,
    `Polyjuice.Client.start_link` should be called to start the client, and
    `Polyjuice.Client.API.stop` should be called to stop it.  Read the
    documentation for `Polyjuice.Client.start_link` for more information.
  - The sync process is started automatically (but can be disabled).
  - A new client does not need to be created when the access token changes
    (e.g. on login)
  - A new `Polyjuice.Client.LowLevel` module has been added for situations
    where the old `Polyjuice.Client` would work better.
- The `Polyjuice.Client.Endpoint.HttpSpec` struct now takes `path` and `query`
  parameters rather than a `url` parameter.  Several helper methods have been
  added for creating the structs.
- `Polyjuice.Client.Endpoint.Proto.http_spec/1` method no longer takes a
  `base_url` parameter.
- `prefix_r0` and `prefix_unstable` have been moved from `Polyjuice.Client` to
  `Polyjuice.Client.Endpoint.HttpSpec`.
- Endpoints that used to return `{:ok}` now return `:ok` instead.

Features:

- Add media upload/download methods. (`Polyjuice.Client.Media`)
- Room requests are queued, to ensure that messages are sent in order.
- New methods for leaving/forgetting rooms. (`Polyjuice.Client.Room.leave` and
  `Polyjuice.Client.Room.forget`)
- The `Polyjuice.Client` client sends events on login and logout.
- Add methods for server discovery using `.well-known`
  (`Polyjuice.Client.WellKnown`)

0.2.3
=====

Features:
- Add support for getting room state (thanks to Pierre de Lacroix)

Bug fixes:

- Fix the sync.

0.2.2
=====

Breaking changes:

- The `Polyjuice.Client.Storage` protocol adds a namespace parameter to the
  key-value storage functions.  Code that uses the function must add this
  parameter to their calls, and modules that implement the protocol must add
  this parameter to their implementations.  Existing stored data should be
  migrated.
- The `Polyjuice.Client.Storage` protocol adds a `kv_del` function.  Modules
  that implement the protocol must add this function to their implementations.
- Client APIs return a different format on errors.

Features:

- Add more filtering methods.
- Support for more endpoints:
  - setting room state
  - getting/streaming room history
- Sync listeners can be functions instead of processes.

0.2.1
=====

Features:

- Add a tutorial.

Bug fixes:

- Fix ETS storage so that the sync process can write to it.

0.2.0
=====

Breaking changes:

- The `Polyjuice.Client` struct has some extra fields that are required by some
  functions.
- The `Polyjuice.Client.API.sync_child_spec` function no longer takes a storage
  as a separate argument -- it is passed in as part of the client.
- Functions for sending events were moved from `Polyjuice.Client` to
  `Polyjuice.Client.Room`, and the order of arguments was changed.

Features:

- New methods and endpoint for setting filters, as well as support in sync.
- Sync sends more messages to update on more of the sync result.
- New function for joining rooms.

0.1.0
====

Initial release.
