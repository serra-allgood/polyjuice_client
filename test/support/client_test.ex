defmodule Polyjuice.ClientTest.Httpd.LoggedOut do
  # just tells the user that they were logged out, no matter what
  def _matrix(session_id, _env, _input) do
    :mod_esi.deliver(
      session_id,
      'Status: 401 Unauthorized\r\nContent-Type: application/json\r\n\r\n{"errcode":"M_UNKNOWN_TOKEN","error":"Unknown token"}'
    )
  end
end
