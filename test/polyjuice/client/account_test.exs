# Copyright 2020 Multi Prise <multiesunhappydev@gmail.com>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

defmodule Polyjuice.Client.AccountTest do
  use ExUnit.Case
  doctest Polyjuice.Client.Account

  test "get account data" do
    with client = %DummyClient{
           response: {
             %Polyjuice.Client.Endpoint.GetAccountData{
               user_id: "@toto:kazarma.local",
               type: "name"
             },
             {:ok, %{name: "toto"}}
           }
         } do
      {:ok, %{name: name}} =
        Polyjuice.Client.Account.get_data(client, "@toto:kazarma.local", "name")

      assert name == "toto"
    end

    with client = %DummyClient{
           response: {
             %Polyjuice.Client.Endpoint.GetAccountData{
               user_id: "@alice:example.org",
               type: "name"
             },
             {:ok, %{name: "toto"}}
           }
         } do
      {:ok, %{name: name}} = Polyjuice.Client.Account.get_data(client, "name")

      assert name == "toto"
    end
  end

  test "put account data" do
    with client = %DummyClient{
           response: {
             %Polyjuice.Client.Endpoint.PutAccountData{
               user_id: "@toto:kazarma.local",
               type: "name",
               account_data: %{name: "marc"}
             },
             {:ok, %{}}
           }
         } do
      {:ok, %{}} =
        Polyjuice.Client.Account.put_data(client, "@toto:kazarma.local", "name", %{name: "marc"})
    end

    with client = %DummyClient{
           response: {
             %Polyjuice.Client.Endpoint.PutAccountData{
               user_id: "@alice:example.org",
               type: "name",
               account_data: %{name: "marc"}
             },
             {:ok, %{}}
           }
         } do
      {:ok, %{}} = Polyjuice.Client.Account.put_data(client, "name", %{name: "marc"})
    end
  end

  test "whoami" do
    with client = %DummyClient{
           response: {
             %Polyjuice.Client.Endpoint.GetWhoAmI{},
             {:ok,
              %{
                "user_id" => "@alice:example.org",
                "device_id" => "ABCDEFG"
              }}
           }
         } do
      assert Polyjuice.Client.Account.whoami(client) ==
               {:ok,
                %{
                  "user_id" => "@alice:example.org",
                  "device_id" => "ABCDEFG"
                }}
    end
  end
end
