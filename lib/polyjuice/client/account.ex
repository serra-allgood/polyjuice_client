# Copyright 2020 Multi Prise <multiestunhappydev@gmail.com>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

defmodule Polyjuice.Client.Account do
  @moduledoc """
  Account related functions.
  """

  @doc """
  Get the data on an account.

  `user_id` is the user whose data is to be queried; if not specified, defaults
  to the user represented by `client_api`. `event_type` is the type of event
  queried.
  """
  @spec get_data(
          client_api :: Polyjuice.Client.API.t(),
          user_id :: String.t() | nil,
          event_type :: String.t()
        ) :: {:ok, map()} | any
  def get_data(client_api, user_id \\ nil, event_type)
      when (is_binary(user_id) or user_id == nil) and is_binary(event_type) do
    Polyjuice.Client.API.call(
      client_api,
      %Polyjuice.Client.Endpoint.GetAccountData{
        user_id: user_id || Polyjuice.Client.API.get_user_and_device(client_api) |> elem(0),
        type: event_type
      }
    )
  end

  @doc """
  Put the data on an account.

  `user_id` is the user whose data is to be set; if not specified, defaults to
  the user represented by `client_api`. `event_type` is type of event to set and
  `data` is the new data.
  """
  @spec put_data(
          client_api :: Polyjuice.Client.API.t(),
          user_id :: String.t() | nil,
          event_type :: String.t(),
          data :: Map.t()
        ) :: {:ok} | any
  def put_data(client_api, user_id \\ nil, event_type, data)
      when (is_binary(user_id) or user_id == nil) and is_binary(event_type) and is_map(data) do
    Polyjuice.Client.API.call(
      client_api,
      %Polyjuice.Client.Endpoint.PutAccountData{
        user_id: user_id || Polyjuice.Client.API.get_user_and_device(client_api) |> elem(0),
        type: event_type,
        account_data: data
      }
    )
  end

  @doc """
  Who am I
  """
  @spec whoami(client :: Polyjuice.Client.LowLevel.t()) :: {:ok, String.t()} | any
  def whoami(client) do
    Polyjuice.Client.API.call(
      client,
      %Polyjuice.Client.Endpoint.GetWhoAmI{}
    )
  end
end
