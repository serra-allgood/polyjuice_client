# Copyright 2019 Hubert Chathi <hubert@uhoreg.ca>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

defmodule Polyjuice.Client.Endpoint.PostUserFilter do
  @moduledoc """
  Upload a filter definition.

  https://matrix.org/docs/spec/client_server/r0.5.0#post-matrix-client-r0-user-userid-filter
  """

  @type t :: %__MODULE__{
          user_id: String.t(),
          filter: map
        }

  @enforce_keys [:user_id, :filter]
  defstruct [
    :user_id,
    :filter
  ]

  defimpl Polyjuice.Client.Endpoint.Proto do
    def http_spec(%{
          user_id: user_id,
          filter: filter
        }) do
      e = &URI.encode_www_form/1
      body = Jason.encode_to_iodata!(filter)

      Polyjuice.Client.Endpoint.HttpSpec.post(
        :r0,
        "user/#{e.(user_id)}/filter",
        body: body
      )
    end

    def transform_http_result(req, status_code, resp_headers, body) do
      Polyjuice.Client.Endpoint.parse_response(req, status_code, resp_headers, body)
    end
  end

  defimpl Polyjuice.Client.Endpoint.BodyParser do
    def parse(_req, parsed) do
      {:ok, Map.get(parsed, "filter_id")}
    end
  end
end
